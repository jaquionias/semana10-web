import React, { useEffect, useState } from "react";

function DevForm({ onSubmit }) {

    const [github_username, setGithubUsername] = useState('');
    const [techs, setTechs] = useState('');
    const [latitude, setLatitude] = useState('');
    const [longitude, setLongitude] = useState('');

    useEffect(() => {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                const { latitude, longitude } = position.coords;
                setLatitude(latitude);
                setLongitude(longitude);
            },
            (err) => {
                console.log(err);
            },
            {
                timeout: 30000,
            },
        );
    }, []);

    async function handleSubmit(e) {
        e.preventDefault();

        await onSubmit({
            github_username,
            techs,
            latitude,
            longitude,
        });

        setGithubUsername('');
        setTechs('');

    }

    return (
        <form onSubmit={ handleSubmit }>
            <div className="input-block">
                <label htmlFor="github_username">Usuário do Github</label>
                <input value={ github_username } onChange={ e => setGithubUsername(e.target.value) }
                       id="github_username" name="github_username" required />
            </div>
            <div className="input-block">
                <label htmlFor="techs">Tecnologias</label>
                <input onChange={ e => setTechs(e.target.value) } id="techs" name="techs" required />
            </div>
            <div className="input-group">
                <div className="input-block">
                    <label htmlFor="latitude">Latitude</label>
                    <input onChange={ e => setLatitude(e.target.value) } type="number" id="latitude"
                           name="latitude" required value={ latitude } />
                </div>
                <div className="input-block">
                    <label htmlFor="longitude">Longitude</label>
                    <input onChange={ e => setLongitude(e.target.value) } type="number" id="longitude"
                           name="longitude" required value={ longitude } />
                </div>
            </div>
            <button type="submit">Salvar</button>
        </form>
    );
}

export default DevForm;