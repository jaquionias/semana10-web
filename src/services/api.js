import axios from 'axios';

const api = axios.create({
    baseURL: 'http://apidevradar.tijack.com.br',
});

export default api;
